# ---- BASH ----
alias ff='find . -type f -name'
alias fd='find . -type d -name'

# ---- GIT ----
alias g='git'

# ---- ANSIBLE ----
venv-activate() {
	if [ -z "$1" ]; then
		echo -e "Error: Must specify virtual env\nExample:\n\tvenv-activate ansible";
		return 1
	fi
	
	source "$HOME/python3-venvs/$1/bin/activate"
} 


# ---- DOCKER ----
# Docker alias and functions
alias d='docker'
alias dstart='docker start'
alias dstop='docker stop'
alias dst='docker stats'
alias di='docker info'
alias dl='docker logs'
alias dsy='docker system'
alias dsyp='docker system prune'

docker_rm_not_tagged() {
  for nottagged in $(docker images -qa -f 'dangling=true')
  do
    docker image rm $nottagged
  done
}

# Docker-compose alias and functions
alias c='docker-compose'
alias cb='docker-compose build'
alias cu='docker-compose up'
alias cdown='docker-compose down'
alias cs='docker-compose stop'
alias cr='docker-compose rm'
alias cps='docker-compose ps'
alias clogs='docker-compose logs'

crf(){
  docker-compose stop $1
  docker-compose rm --force $1
}
