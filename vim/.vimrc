set nocompatible

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugin Manager
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Autoinstall vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" Plug config
call plug#begin('~/.vim/bundle')
Plug 'tpope/vim-sensible'
Plug 'sheerun/vim-polyglot'           " Collection of language packs
Plug 'junegunn/vim-easy-align'        " Line up text
Plug 'dense-analysis/ale'             " Linting
Plug 'itchyny/lightline.vim'          " Status line
" +-- Git integration
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'airblade/vim-gitgutter'
" +-- Visuals
Plug 'tmux-plugins/vim-tmux-focus-events' " Restores FocusGained and FocusLost in tmux
Plug 'machakann/vim-highlightedyank'      " Highlist yanked text
Plug 'romainl/vim-cool'                   " Auto enable/disable search highlight
" +-- Colors
Plug 'lifepillar/vim-gruvbox8'
Plug 'shinchu/lightline-gruvbox.vim'
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins config 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" +-- EasyAlign
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" +-- Lightline
let g:lightline = {}
let g:lightline.colorscheme = 'gruvbox'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around and searching
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"set hlsearch            " highlight matches
"set ignorecase          " ignore case whe using a search pattern
"set smartcase           " be smart about cases

" Press F4 to toggle highlighting on/off, and show current value.
noremap <F4> :set hlsearch! hlsearch?<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Tabs and indenting
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab           " use spaces instead of tabs
set smarttab            " Be smart when using tabs ;)
set shiftwidth=2        " space character for indentation
set tabstop=2           " spaces to insert when using <TAB>

set autoindent
set smartindent 

set wrap                " Wrap lines
set linebreak           " Wrap at word boundaries, not in the middle of words

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => UI Config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set ruler               " Always show current position
set number              " Show line number
if exists('g:+relativenumber') || (v:version > 700)
  set relativenumber    " relative number to the cursos line 
endif

set cursorline          " highlight current line
set lazyredraw          " redraw only when we need to
set showmatch           " highlight matching brackets
set noeb novb t_vb=     " No annoying sound on error

set laststatus=2        " Enable lightline.vim
set noshowmode          " Disable INSERT line on the bottom

filetype indent on      " load filetype-sepecific indent files

set showcmd             " Show partially-typed commands in the bottom right

" Colorscheme
if &term == "screen"
  set t_Co=256
endif
syntax on
set background=dark
colorscheme gruvbox8_hard

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Folding
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set foldenable          " enable folding
set foldmethod=syntax   " fold based on indent level
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max

" space open/close folds
nnoremap <leader>z za

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Leader Shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = "\<Space>"    " leader is space bar

" Remap VIM 0 to first non-blank character
nmap <leader>0 ^

map <leader>pp :setlocal paste!<cr>

" +-- Saving and quiting
nmap <leader>ww :w<cr>
nmap <leader>qq :q<cr>
nmap <leader>q! :q!<cr>
" :W sudo saves the file 
"command W w !sudo tee % > /dev/null

" +-- Buffers
nmap <leader>bl :bnext<cr>
nmap <leader>bh :bprevious<cr>
" opens new buffer but expands new directory
nmap <leader>be :e <c-r>=expand("%:p:h")<cr>/
" closes current buffer
nmap <leader>bq :bp <BAR> bd #<CR>

" +-- Windows
" Navigate between windows with CTRL+[hjkl]
nmap <leader>ws :split<cr>
nmap <leader>wv :vsplit<cr>
nmap <leader>wc :close<cr>

" +-- Tabs
" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove 
map <leader>th :tabp<cr>
map <leader>tl :tabn<cr>
" Opens a new tab with the current buffer's path
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/
"
" +-- GitGutter
nmap <leader>gg :GitGutterToggle<cr>
" +-- GV
nmap <leader>gvv :GV<cr>
nmap <leader>gv! :GV!<cr>
nmap <leader>gv? :GV?<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Backups
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Move backups directory or disable enterily
let s:vim_tmp= expand("$HOME/.vim/tmp")
if filewritable(s:vim_tmp) == 0 && exists("*mkdir")
  call mkdir(s:vim_tmp, "p", 0700)
endif
if filewritable(s:vim_tmp) != 0
  "set backup
  let &backupdir=s:vim_tmp . '//,.'
  let &directory=s:vim_tmp . '//,.'
  set viminfo+=n~/.vim/.viminfo " Change viminfo folder and file size
else
  set noswapfile
endif

" Persist undo state across sessions
" https://www.reddit.com/r/vim/comments/2ib9au/why_does_exiting_vim_make_the_next_prompt_appear/cl0zb7m/
let s:vim_cache = expand("$HOME/.vim/undo")
if filewritable(s:vim_cache) == 0 && exists("*mkdir")
  call mkdir(s:vim_cache, "p", 0700)
endif
if filewritable(s:vim_cache) != 0
  set undofile
  let &undodir=s:vim_cache
  set undolevels=1000
  set undoreload=10000
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Various
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set hidden                     " A buffer becomes hidden when it is abandoned
set encoding=utf8              " Set utf8 as standard encoding and es the standard language
set ffs=unix,dos,mac           " Use Unix as the standard file type
set backspace=indent,eol,start " make the backspace work like most programs

" Make Vim respond faster to some stuff, e.g. vim-gitgutter load delay
"set updatetime=250

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
